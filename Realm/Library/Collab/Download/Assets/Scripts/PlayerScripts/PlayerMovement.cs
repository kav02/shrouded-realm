﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PlayerMovement : MonoBehaviour
{
    private GameObject Inventory;
    public GameObject Hand;
    float speed = 3f;
    
    readonly float rotSpeed = 80f;
    float rot = 0f;
    float gravity = 3f;
    public float JumpForce = 7f;
    bool isGrounded = false;
    public float GravityScale = 0.5f;

    public bool Wielded = false;
    public bool CanSwing = false;
    public string currentWield = null;

    private bool CastingSpell = false;

 

    Rigidbody rb;

    Vector3 moveDir = Vector3.zero;

    CharacterController controller;
    Animator anim;

    // Start is called before the first frame update
    void Start()
    {
        controller = GetComponent<CharacterController> ();
        anim = GetComponent<Animator> ();
        rb = GetComponent<Rigidbody>();
        Inventory = this.transform.Find("Inventory").gameObject;
    }

    void CastSpell()
    {
        int Cost = this.GetComponent<PlayerLevels>().Cost;
        string currentSpell = this.GetComponent<PlayerLevels>().Currentspell;


        if (Cost < this.GetComponent<Stats>().Magix && !CastingSpell)
        {
            StartCoroutine(SpellCooldown());
            Vector3 target = MousePoint();
            if(target != Vector3.zero)
            {
                
                
                GameObject Spell = Resources.Load<GameObject>("Spells/" + currentSpell);
                GameObject SpellInstance = Instantiate(Spell);
                SpellInstance.GetComponent<TravelTowards>().Target = target;
                SpellInstance.transform.position = Hand.transform.position;
                this.GetComponent<Stats>().Magix = this.GetComponent<Stats>().Magix - Cost;
                
            }
        }
        else
        {
            Debug.Log("Player is lacking magix, or already casting a spell.");
        }
    }

    void Wield()
    {
        anim.SetLayerWeight(1, 0.9f);
        if (Input.GetMouseButtonDown(0) & CanSwing)
            if (!EventSystem.current.IsPointerOverGameObject())
            {
                {
                    StartCoroutine(Swing());
                    
                }
            }
    }

    public void Unwield()
    {
        if (Wielded && Hand.transform.Find(currentWield).gameObject.GetComponent<ToolPosition>().Swinging == false)
        {
            Wielded = false;
            CanSwing = false;
            Inventory.GetComponent<InventorySystem>().Pickup(currentWield, null);
            Destroy(Hand.transform.Find(currentWield).gameObject);
            currentWield = null;
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (Wielded)
        {
            Wield();
        }
        else
        {
            anim.SetLayerWeight(1, 0);
        }

        if (Input.GetKey(KeyCode.Q))
        {

            CastSpell();
        }

        if (Input.GetKey(KeyCode.U))
        {

            Unwield();
        }

        if (Input.GetKey(KeyCode.LeftShift))
        {
            anim.SetBool("IsRunning", true);
            speed = 5f;
        }
        if (Input.GetKeyUp(KeyCode.LeftShift)){
            anim.SetBool("IsRunning", false);
            speed = 3f;
        }
        if (Input.GetKeyUp(KeyCode.W) || Input.GetKeyUp(KeyCode.S) || controller.isGrounded == false)
        {
            anim.SetInteger("IsWalking", 0);
            moveDir.z = 0;
            moveDir.x = 0;
        }

        if (Input.GetKey(KeyCode.W))
        {
            anim.SetInteger("IsWalking", 1);
            moveDir.z = 1;
            moveDir.z *= speed;
            moveDir.x = 0;
            moveDir = transform.TransformDirection(moveDir);
        }

        if (Input.GetKey(KeyCode.S))
        {
            anim.SetInteger("IsWalking", -1);
            moveDir.z = -1;
            moveDir.z *= speed;
            moveDir.x = 0;
            moveDir = transform.TransformDirection(moveDir);

        }

        if (isGrounded)
        {
            anim.SetBool("IsFalling", false);
            moveDir.y = -5f;
            if (Input.GetKey(KeyCode.Space))
            {
//                isJumping = true;
                Debug.Log("JUMP");
                moveDir.y = JumpForce;
            }

        }
//        if (controller.isGrounded)
 //       {
  //          isJumping = false;
   //     }
        if (isGrounded == false)
        {
            anim.SetBool("IsFalling", true);
            moveDir.y = moveDir.y + ((-gravity) * GravityScale * Time.deltaTime);
        }

        rot += Input.GetAxis("Horizontal") * rotSpeed * Time.deltaTime;
        anim.SetFloat("TurnDirection", Input.GetAxis("Horizontal"));
        transform.eulerAngles = new Vector3(0, rot, 0);

        //moveDir.y -= gravity * Time.deltaTime;

        controller.Move (moveDir * Time.deltaTime);
    }

    private void FixedUpdate()
    {
        RaycastHit hit;
        Vector3 dwd = transform.TransformDirection(Vector3.down);
        if (Physics.Raycast(transform.position, dwd, out hit))
        {
            if (hit.distance < 0.5) {
                isGrounded = true;
            } else
            {
                isGrounded = false;
            }
        }

    }

    IEnumerator Swing()
    {
        anim.SetBool("IsAttacking", true);
        Hand.transform.Find(currentWield).gameObject.GetComponent<ToolPosition>().Swinging = true;
        yield return new WaitForSeconds(1);
        anim.SetBool("IsAttacking", false);
        Hand.transform.Find(currentWield).gameObject.GetComponent<ToolPosition>().Swinging = false;
    }

    IEnumerator SpellCooldown()
    {
        CastingSpell = true;
        StartCoroutine(MagicAttackAnim());
        yield return new WaitForSeconds(2);
        CastingSpell = false;
    }

    IEnumerator MagicAttackAnim()
    {
        anim.SetLayerWeight(anim.GetLayerIndex("CastSpell"), 1);
        anim.SetBool("AttackSpell", true);
        yield return new WaitForSeconds(0.5f);
        anim.SetBool("AttackSpell", false);
        anim.SetLayerWeight(2, 0);

    }

    private Vector3 MousePoint()
    {

        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit))
        {
            return hit.point;
        }
        return Vector3.zero;
    }
}
