﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[System.Serializable]
public class CollectSaveData
{
    public float rotation;
    public float[] position;
    public int Scene;
    //basic stats
    public int Health;
    public int Magix;

    //levels
    public int CombatLVL;
    public int MagixLVL;
    public int MiningLVL;
    public int TreecuttingLVL;
    public int ExplorationLVL;
    
    //Inventory
    public string[] ITInventory;
    public int[] QTYInventory;



    public CollectSaveData(InventorySystem invent, Stats plstats, PlayerLevels plLevels, Transform playertransform)
    {
        position = new float[3];
        position[0] = playertransform.position.x;
        position[1] = playertransform.position.y;
        position[2] = playertransform.position.z;

        rotation = playertransform.rotation.y;

        ITInventory = invent.ITInventoryArr;
        QTYInventory = invent.QTYInventoryArr;

        Health = plstats.Health;
        Magix = plstats.Magix;
        Scene = SceneManager.GetActiveScene().buildIndex;

        CombatLVL = plLevels.CombatLvl;
        MagixLVL = plLevels.MagixLvl;
        MiningLVL = plLevels.MiningLvl;
        TreecuttingLVL = plLevels.TreecuttingLvl;
        ExplorationLVL = plLevels.ExplorationLvl;
    }

}
