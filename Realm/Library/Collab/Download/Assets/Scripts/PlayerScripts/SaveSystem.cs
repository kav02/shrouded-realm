﻿using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public static class SaveSystem
{
    public static void SavePlayer()
    {
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/player.realm";
        FileStream stream = new FileStream(path, FileMode.Create);
        GameObject player = PlayerManager.instance.Player;

        CollectSaveData data = new CollectSaveData(player.transform.Find("Inventory").GetComponent<InventorySystem>(), player.GetComponent<Stats>(), player.GetComponent<PlayerLevels>(), player.transform);

        formatter.Serialize(stream, data);

        Debug.Log(path);
        stream.Close();
    }

    public static CollectSaveData LoadPlayer()
    {
        string path = Application.persistentDataPath + "/player.realm";
        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            CollectSaveData data = formatter.Deserialize(stream) as CollectSaveData;
            stream.Close();

            return data;
        }
        else
        {
            Debug.LogError("Save file not found in" + path);
            return null;
        }
    }

}
