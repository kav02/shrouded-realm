﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLevels : MonoBehaviour
{
    [SerializeField]
    private int OverallLvl = 1;

    public int MagixLvl = 1;
    public int CombatLvl = 1;
    public int MiningLvl = 1;
    public int TreecuttingLvl = 1;
    public int ExplorationLvl = 1;

    public int CalculateOverallLevel()
    {
        int CalculatedLevel = (int)(Mathf.Floor((CombatLvl + MagixLvl) / 2) + Mathf.Floor(MiningLvl / 3) + Mathf.Floor(ExplorationLvl / 5));
        OverallLvl = CalculatedLevel;
        return CalculatedLevel;
    }
}
