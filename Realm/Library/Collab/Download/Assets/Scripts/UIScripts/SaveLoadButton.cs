﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SaveLoadButton : MonoBehaviour
{

    public void SaveGame()
    {
        SaveSystem.SavePlayer();

    }

    public void CheckSceneSave()
    {
        CollectSaveData data = SaveSystem.LoadPlayer();
        if (SceneManager.GetActiveScene().buildIndex == data.Scene)
        {
            LoadGame();
        }
        else
        {
            Debug.Log("Player is in the wrong scene, changing scene..");
            SceneManager.LoadScene(data.Scene);
        }
    }

    public void LoadGame()
    {

        CollectSaveData data = SaveSystem.LoadPlayer();


        GameObject player = PlayerManager.instance.Player;

        //loading in player stats.
        Stats playerstats = player.transform.GetComponent<Stats>();
        playerstats.Health = data.Health;
        playerstats.Magix = data.Magix;

        //loading in player levels.
        PlayerLevels playerlevels = player.transform.GetComponent<PlayerLevels>();
        playerlevels.CombatLvl = data.CombatLVL;
        playerlevels.MagixLvl = data.MagixLVL;
        playerlevels.MiningLvl = data.MiningLVL;
        playerlevels.TreecuttingLvl = data.TreecuttingLVL;
        playerlevels.ExplorationLvl = data.ExplorationLVL;

        //loading in player inventory.
        InventorySystem plInventory = player.transform.Find("Inventory").GetComponent<InventorySystem>();
        plInventory.ITInventoryArr = data.ITInventory;
        plInventory.QTYInventoryArr = data.QTYInventory;
        plInventory.UpdateImages();

        //positioning player
        player.transform.position = new Vector3(data.position[0], data.position[1], data.position[2]);
        player.transform.rotation = Quaternion.Euler(0, data.rotation, 0);

    }
}
