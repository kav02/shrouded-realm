﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class TileClicked : MonoBehaviour
{
    public int Index = 0;
    private GameObject Inventory;
    public GameObject Hand;
    private GameObject Player;

    private void Start()
    {
        Player = PlayerManager.instance.Player;
        Inventory = Player.transform.Find("Inventory").gameObject;
        
    }

    void attachItemToHand(string ItemName, bool canSwing)
    {
        if (!string.IsNullOrEmpty(Player.GetComponent<PlayerMovement>().currentWield))
        {
            Debug.Log("Player is already wielding a weapon.");
            Player.GetComponent<PlayerMovement>().Unwield();
        }

        GameObject item = Resources.Load<GameObject>("Tools/" + ItemName);
        GameObject copy = Instantiate(item, Hand.transform);
        ToolPosition properties = copy.GetComponent<ToolPosition>();
        copy.transform.localPosition = properties.Pos;
        copy.transform.localEulerAngles = properties.Rot;
        copy.transform.localScale = properties.Sca;
        copy.transform.name = ItemName;

        Player.GetComponent<PlayerMovement>().Wielded = true;
        Player.GetComponent<PlayerMovement>().CanSwing = canSwing;
        Player.GetComponent<PlayerMovement>().currentWield = ItemName;

        Inventory.GetComponent<InventorySystem>().Drop(Index);

    }

    public void Clicked()
    {
        string item = Inventory.GetComponent<InventorySystem>().ITInventoryArr[Index];
        string path = "Assets/Resources/Tools/" + item + ".prefab";
        if (File.Exists(path))
        {
            Debug.Log("Player has equipped " + item);
            attachItemToHand(item, true);
        }
        /*
            string item = Inventory.GetComponent<InventorySystem>().ITInventoryArr[Index];
        switch (item)
        {
            case "Sword":
                Debug.Log("Player has equipped sword");
                attachItemToHand(item, true);
                break;
            case "Pickaxe":
                Debug.Log("Player has equipped pickaxe");
                attachItemToHand(item, true);
                break;
            default:
                Debug.Log("Item is unusable");
                break;

        }
        */
    }
}
