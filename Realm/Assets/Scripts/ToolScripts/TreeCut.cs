﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeCut : MonoBehaviour
{
    public int Damage;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Tree" && this.GetComponent<ToolPosition>().Swinging == true)
        {
            if(PlayerManager.instance.Player.GetComponent<PlayerLevels>().TreecuttingLvl >= other.GetComponent<Tree>().LevelReq)
            {
                other.GetComponent<Tree>().Bash(Damage);
            }
            else
            {
                Debug.Log("Player lacks level to cut this tree.");
                OutputGUI.ShowPlayer("you lack the level to cut this tree.");
            }
        }
    }
}
