﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Potion : MonoBehaviour
{
    public int AddHealth;
    public int AddMagix;
    void Start()
    {
        GameObject Player = PlayerManager.instance.Player;
        Player.GetComponent<PlayerMovement>().UseItem();
        Stats stats = Player.GetComponent<Stats>();
        stats.Heal(AddHealth);
        stats.RegenMagix(AddMagix);
    }

}
