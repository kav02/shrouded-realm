﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickaxe : MonoBehaviour
{

    public int HitDamage = 15;
    private void Start()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "CanMine" && this.GetComponent<ToolPosition>().Swinging == true)
        {
            OreType OtherOre = other.GetComponent<OreType>();
            OtherOre.Damage(HitDamage);
            this.transform.Find("Sparks").gameObject.SetActive(true);
            this.transform.Find("Sparks").GetComponent<ParticleSystem>().Play();
        }
    }
}
