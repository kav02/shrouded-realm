﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToolAttack : MonoBehaviour
{

    public int EstDamage;
    public int RequiredLevel = 1;
    private PlayerLevels playerlevels;
    

        private void Start()
    {


        playerlevels = PlayerManager.instance.Player.GetComponent<PlayerLevels>();
        if (playerlevels.CombatLvl < RequiredLevel)
        {
            Debug.Log("Player lacks combat level to equip this weapon.");
            PlayerManager.instance.Player.GetComponent<PlayerMovement>().Unwield("you need to be level " + RequiredLevel + " combat to use this.");
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Hostile")
        {
            Debug.Log("Has hit an enemy!");

            int damage = Random.Range(EstDamage - 5, EstDamage + 5);
            Debug.Log(damage);

            //if health is above 0, but with this last hit it will be below 0... give XP to player!
            if ((other.GetComponent<Stats>().Health - damage) < 0 && other.GetComponent<Stats>().Health > 0)
            {
                Debug.Log("Player has killed " + other.transform.name);
                OutputGUI.ShowPlayer("you killed " + other.transform.name);
                playerlevels.AddCombat(other.GetComponent<Stats>().XPWorth);
            }

            other.GetComponent<Stats>().Damage(damage);

        } else if(other.tag == "NPC" && this.GetComponent<ToolPosition>().Swinging == true)
        {
            OutputGUI.ShowPlayer("You can't attack " + other.name + "!");
        }
    }
}
