﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IfNotActive : MonoBehaviour
{
    public GameObject Check;
    

    // Update is called once per frame
    void Update()
    {
        if(Check.activeSelf == false)
        {
            if (this.transform.Find("NextEvent"))
            {
                this.transform.Find("NextEvent").gameObject.SetActive(true);
                Destroy(this);
            }
        }
    }
}
