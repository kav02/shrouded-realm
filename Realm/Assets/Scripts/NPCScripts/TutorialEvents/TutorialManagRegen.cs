﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialManagRegen : MonoBehaviour
{
    private void Update()
    {
        if(this.GetComponent<Stats>().Magix < 20)
        {
            this.GetComponent<Stats>().RegenMagix(30);
            OutputGUI.ShowPlayer("You are granted more magix being as this is a tutorial.");
        }
    }
}
