﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialManager : MonoBehaviour
{
    private GameObject Player;
    public bool isTouchSensitive;
    public bool isTalkSensitive;
    // Start is called before the first frame update
    void Start()
    {
        Player = PlayerManager.instance.Player;
    }

    // Update is called once per frame
    void Update()
    {
        if (Vector3.Distance(Player.transform.position, this.transform.position) < 5 & isTalkSensitive)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                if (this.transform.Find("NextEvent"))
                {
                    this.transform.Find("NextEvent").gameObject.SetActive(true);
                }
            }
        }
    }

    private void OnTriggerEnter(Collider collision)
    {
        if (isTouchSensitive && collision.tag == "Player")
        {
            if (this.transform.Find("NextEvent"))
            {
                this.transform.Find("NextEvent").gameObject.SetActive(true);
            }
        }
    }
}
