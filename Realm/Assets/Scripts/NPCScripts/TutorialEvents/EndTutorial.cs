﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndTutorial : MonoBehaviour
{
    public Vector3 StartPos;
    public float StartRot;
    public int nextIndex;
    GameObject Player;

    private bool PickedUp = false;

    private void Start()
    {
        Player = PlayerManager.instance.Player;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player" & !PickedUp)
        {
            PickedUp = true;
            Player.transform.Find("Inventory").GetComponent<InventorySystem>().ClearInventory();
            Player.transform.Find("Inventory").GetComponent<InventorySystem>().Pickup("WoodenSword", null, null);
            SaveSystem.SavePlayer(nextIndex, StartPos, StartRot);
            StartCoroutine(Change());
        }
    }

    IEnumerator Change()
    {
        GUIManager.instance.UICanvas.GetComponent<TransitionLayer>().FadeOut();
        yield return new WaitForSeconds(2);
        SceneManager.LoadScene(nextIndex);

    }
}
