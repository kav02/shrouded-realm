﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialDummy : MonoBehaviour
{
    Animator anim;
    Stats mobstat;

    private bool isDead = false;
    private string Drop = "HumanBone";
    // Start is called before the first frame update
    void Start()
    {
        anim = this.transform.Find("Humanoid").GetComponent<Animator>();
        mobstat = this.GetComponent<Stats>();
    }

    // Update is called once per frame
    void Update()
    {
        if (mobstat.Health <= 0 & !isDead)
        {
            isDead = true;
            Die();
        }
    }

    void Die()
    {
        anim.SetLayerWeight(1, 0);
        anim.SetBool("IsChasing", false);
        anim.SetBool("IsDead", true);
        anim.SetBool("isAttacking", false);
        PlayerManager.instance.Player.transform.Find("Inventory").GetComponent<InventorySystem>().Pickup(Drop, null, null);
        StartCoroutine(Cleanup());
    }

    IEnumerator Cleanup()
    {
        Debug.Log("started");
        yield return new WaitForSeconds(5);
        this.gameObject.SetActive(false);
    }
}
