﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class HumanControl : MonoBehaviour
{
    public bool isBored = false;
    public bool isSat = false;
    public bool isWalker = false;
    public bool isHoldingTorch = false;
    public Transform[] WalkingPoints;
    public float SenseRadius = 3.0f;
    public float WaitBetweenWalks = 5.0f;
    public Transform FaceWhilstWait = null;

    private NavMeshAgent agent;
    private Animator anim;
    private int WalkNum = 0;
    private Transform currentTarget;

    void Start()
    {

        anim = this.transform.Find("Humanoid").GetComponent<Animator>();

        if (isHoldingTorch)
        {
            anim.SetLayerWeight(1, 1);
        }

        if (isSat)
        {
            anim.SetBool("isSitting", true);
            //this human only sits.
        }

        if (isBored)
        {
            anim.SetBool("isBored", true);
            //this human is bored.
        }

        if (isWalker)
        {
            agent = GetComponent<NavMeshAgent>();
            if (WalkNum == WalkingPoints.Length)
            {
                WalkNum = 0; //reset walking!
            }
            StartCoroutine(WalkToTarget(WalkingPoints[WalkNum]));
            WalkNum++;
            //this human walks!
        }


    }

    void Update()
    {
        if (isWalker)
        {
            float distance = Vector3.Distance(currentTarget.position, transform.position);
            //Debug.Log(distance);
            if (distance < SenseRadius)
            {
                //Debug.Log("Stopping");
                anim.SetBool("isWalking", false);
                agent.isStopped = true;
                FaceTarget(FaceWhilstWait);
                Start();
            }
        }
    }

    void FaceTarget(Transform target)
    {
        if (target)
        {
            Vector3 direction = (target.position - transform.position).normalized;
            Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
            transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 5f);
        }
    }

    IEnumerator WalkToTarget(Transform target)
    {
        currentTarget = target;
        yield return new WaitForSeconds(WaitBetweenWalks);
        agent.isStopped = false;
        agent.SetDestination(target.position);
        FaceTarget(target);
        anim.SetBool("isWalking", true);
    }
}
