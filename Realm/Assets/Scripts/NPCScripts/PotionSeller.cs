﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PotionSeller : MonoBehaviour
{
    public bool isPotionShop = false;

    private GameObject Player;
    private GameObject GUI;

    // Start is called before the first frame update
    void Start()
    {
        Player = PlayerManager.instance.Player;
        GUI = GUIManager.instance.UICanvas.transform.Find("GameUI").gameObject;
        GUI.GetComponent<ShowHide>().Menu = this.gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        if (Vector3.Distance(Player.transform.position, this.transform.position) < 5)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                GUI.GetComponent<ShowHide>().PotionShopEnable();
                isPotionShop = true;
            }
        } else if(Vector3.Distance(Player.transform.position, this.transform.position) >= 5 && isPotionShop)
        {
            GUI.GetComponent<ShowHide>().PotionShopDisable();
            isPotionShop = true;
        }
    }
}
