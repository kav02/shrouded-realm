﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using TMPro;
using UnityEngine;

public class PotionShop : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public int price;
    public string potion;
    private GameObject Descriptor;

    private GameObject Player;

    private void Start()
    {
        Player = PlayerManager.instance.Player;
        Descriptor = GUIManager.instance.UICanvas.transform.Find("GameUI").transform.Find("Descriptor").gameObject;
    }
    public void Buy()
    {
        if (Player.GetComponent<PlayerLevels>().Purchase(price))
        {
            Player.transform.Find("Inventory").GetComponent<InventorySystem>().Pickup(potion, null, null);
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        TextMeshProUGUI Text = Descriptor.transform.Find("Textbox").GetComponent<TextMeshProUGUI>();
        TextMeshProUGUI Title = Descriptor.transform.Find("Titlebox").GetComponent<TextMeshProUGUI>();
        RectTransform DescriptorTrans = Descriptor.GetComponent<RectTransform>();

            Title.text = potion;
            Title.color = Color.black;
            Text.text = "Click to buy a " + potion + ", it costs " + price + ".";

        

        DescriptorTrans.position = Input.mousePosition - new Vector3((DescriptorTrans.sizeDelta.x) / 1.8f, 0, 0);
        Descriptor.SetActive(true);

    }

    public void OnPointerExit(PointerEventData eventData)
    {
        Descriptor.SetActive(false);
    }
}
