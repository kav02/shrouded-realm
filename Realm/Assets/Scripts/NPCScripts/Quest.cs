﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class Quest : MonoBehaviour
{
    public string Title;
    public string Description;
    public string Requirement;
    public int Quantity;
    public string Reward = "WoodenSword";
    public int CoinReward = 1;
    public bool isStarted = false;
    public bool isCompleted = false;
    private GameObject player;

    private void Start()
    {
        player = PlayerManager.instance.Player;
        LoadData();
    }

    public bool RewardPlayer()
    {
        bool returnvalue = false;
        if (player.transform.Find("Inventory").GetComponent<InventorySystem>().TakeFromPlayer(Requirement, Quantity))
        {
            isCompleted = true;
            returnvalue = true;
            player.GetComponent<PlayerLevels>().GiveGold(CoinReward);
            player.transform.Find("Inventory").GetComponent<InventorySystem>().Pickup(Reward, null, "You were awarded with " + Reward);
            player.GetComponent<QuestCollection>().MarkCompleted(Title);
            QuestCompletionState();
            SaveSystem.SavePlayer(SceneManager.GetActiveScene().buildIndex, PlayerManager.instance.Player.transform.position, PlayerManager.instance.Player.transform.rotation.eulerAngles.y);
        }
        return returnvalue;

    }

    private void LoadData()
    {
        QuestCollection qcollection = player.GetComponent<QuestCollection>();
        qcollection.Start();
        if(qcollection.CheckForQuest(this) != -1)
        {
            //Debug.Log("IT DOES EXIST!");
            isStarted = true;
            isCompleted = qcollection.QuestList[qcollection.CheckForQuest(this)].isCompleted;
            qcollection.QuestList[qcollection.CheckForQuest(this)] = this;
        }
        if (isCompleted)
        {
            QuestCompletionState();
        }

        /*if (data.NumOfQuests != 0)
        {
            for (int index = 0; index < data.QuestNames.Length; index++)
            {
                if(data.QuestNames[index] == Title)
                {
                    isStarted = true;
                    isCompleted = data.QuestCompletions[index];
                    player.GetComponent<QuestCollection>().AddQuest(this);
                    if (isCompleted)
                    {
                        QuestCompletionState();
                    }
                }
            }
        }*/
    }

    void QuestCompletionState()
    {
        if (this.transform.Find("QuestCompleted"))
        {
            this.transform.Find("QuestCompleted").gameObject.SetActive(true);
        }
    }

}
