﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class QuestCollection : MonoBehaviour
{
    public List<Quest> QuestList = new List<Quest>();
    private QuestPage playerUI;

    public void Start()
    {
        

        playerUI = GUIManager.instance.UICanvas.transform.Find("GameUI").Find("QuestsPage").GetComponent<QuestPage>();
        if (SceneManager.GetActiveScene() != SceneManager.GetSceneByName("Tutorial"))
        {
            LoadData();
        }
        playerUI.Start();
        playerUI.UpdateList(QuestList);

    }




    public List<Quest> ReturnList()
    {
        return QuestList;
    }

    public Quest retrieveQuest(int index)
    {
        return QuestList[index];
    }

    public void AddQuest(Quest newquest)
    {
        QuestList.Add(newquest);
        playerUI.Start();
        playerUI.UpdateList(QuestList);
    }

    public void MarkCompleted(string Questtitle)
    {
        foreach(Quest que in QuestList)
        {
            if(Questtitle == que.Title)
            {
                que.isCompleted = true;
                playerUI.UpdateList(QuestList);
            }
        }
    }

    public int CheckForQuest(Quest questtofind)
    {
        //-1 is termination statement.
        int indexquest = -1;
        for(int index = 0; index < QuestList.Count; index++)
        {
            if(questtofind.Title == QuestList[index].Title)
            {
                indexquest = index;
            }
        }
        return indexquest;
    }
    
    public void LoadData()
    {
        //loads in quests previously started by the player.
        CollectSaveData data = SaveSystem.LoadPlayer();
        if(data.NumOfQuests != 0) {
            if (data != null)
            {
                for (int index = 0; index < data.NumOfQuests; index++)
                {
                    //Debug.Log(data.NumOfQuests);
                    //Debug.Log("Adding Quest" + index);
                    Quest newQuest = this.gameObject.AddComponent<Quest>();
                    newQuest.enabled = false;
                    newQuest.Title = data.QuestNames[index];
                    newQuest.Description = data.QuestDescriptions[index];
                    newQuest.isCompleted = data.QuestCompletions[index];
                    if (CheckForQuest(newQuest) == -1)
                    {
                        AddQuest(newQuest);
                    }
//                    Debug.Log("Added quest");

                    Destroy(newQuest);
                }
            }
        }
        
    }

}
