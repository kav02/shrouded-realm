﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChatScript : MonoBehaviour
{
    private GameObject Player;
    private Quest thisquest;
    public string[] chat;
    public string[] questchat;
    public string[] completedchat;
    // Start is called before the first frame update
    void Start()
    {
        Player = PlayerManager.instance.Player;
        if (this.GetComponent<Quest>())
        {
            thisquest = this.GetComponent<Quest>();
        }
        else
        {
            thisquest = null;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Vector3.Distance(Player.transform.position, this.transform.position) < 5)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                if (thisquest == null)
                {
                    Player.GetComponent<Chatting>().StartChat(chat, chat.Length, this.transform.Find("Camera").GetComponent<Camera>());
                }
                else
                {
                    if (thisquest.isCompleted == false && thisquest.isStarted == true)
                    {
                        if (thisquest.RewardPlayer())
                        {
                            Player.GetComponent<Chatting>().StartChat(completedchat, completedchat.Length, this.transform.Find("Camera").GetComponent<Camera>());

                        }
                        else
                        {
                            Player.GetComponent<Chatting>().StartChat(questchat, questchat.Length, this.transform.Find("Camera").GetComponent<Camera>());

                        }
                    }
                    else if (thisquest.isStarted == false && thisquest.isCompleted == false)
                    {
                        Player.GetComponent<Chatting>().StartChat(chat, chat.Length, this.transform.Find("Camera").GetComponent<Camera>());
                        Player.GetComponent<QuestCollection>().AddQuest(thisquest);
                        thisquest.isStarted = true;
                    }
                    else
                    {
                        Player.GetComponent<Chatting>().StartChat(completedchat, completedchat.Length, this.transform.Find("Camera").GetComponent<Camera>());

                    }
                }
            }
        }


    }
}
