﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransitionLayer : MonoBehaviour
{
    private Animator FadeAnimator;

    private void Start()
    {
        FadeAnimator = this.transform.Find("Transition").GetComponent<Animator>();
        this.transform.Find("Transition").gameObject.SetActive(true);
        FadeIn();
    }

    public void FadeIn()
    {
        FadeAnimator.SetBool("FadingIn", true);
    }

    public void FadeOut()
    {
        FadeAnimator.SetBool("FadingIn", false);
        FadeAnimator.SetBool("FadingOut", true);
    }
}
