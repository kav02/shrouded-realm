﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SaveLoadButton : MonoBehaviour
{
    
    public void SaveGame()
    {
        SaveSystem.SavePlayer(SceneManager.GetActiveScene().buildIndex, PlayerManager.instance.Player.transform.position, PlayerManager.instance.Player.transform.rotation.eulerAngles.y);
        UnityEngine.Debug.Log(PlayerManager.instance.Player.transform.rotation.eulerAngles.y);
    }

    public void CheckSceneSave()
    {
        CollectSaveData data = SaveSystem.LoadPlayer();
        if (SceneManager.GetActiveScene().buildIndex == data.Scene)
        {
            Load();
        }
        else
        {
            UnityEngine.Debug.Log("Player is in the wrong scene, changing scene..");
            SceneManager.LoadScene(data.Scene);
        }
    }

    public void ExitGame()
    {
        SaveGame();
        Application.OpenURL(Application.persistentDataPath);
        Application.OpenURL("https://www.surveymonkey.co.uk/r/GH9XGW2");
        Application.Quit();
    }

    public void Load()
    {

        SaveSystem.LoadGame();

    }
}
