﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.SceneManagement;


public class MainMenu : MonoBehaviour
{

    public void BeginGame()
    {
        if(SaveSystem.LoadPlayer() == null)
        {
            StartCoroutine(Transfer(0));
        }
        else
        {
            this.transform.Find("MainOptions").gameObject.SetActive(false);
            this.transform.Find("Validation").gameObject.SetActive(true);
        }
    }

    public void OptionYes()
    {
        StartCoroutine(Transfer(0));
    }

    public void OptionNo()
    {
        this.transform.Find("MainOptions").gameObject.SetActive(true);
        this.transform.Find("Validation").gameObject.SetActive(false);
    }

    public void ContinueGame()
    {
        if(SaveSystem.LoadPlayer() != null)
        {
            StartCoroutine(Transfer(1));
        }
        else
        {
            StartCoroutine(Transfer(0));
        }

    }

    public void ExitGame()
    {
        StartCoroutine(Transfer(2));
    }

    IEnumerator Transfer(int Option)
    {
        this.transform.parent.GetComponent<TransitionLayer>().FadeOut();
        yield return new WaitForSeconds(2);
        switch (Option)
        {
            case 0:
                SceneManager.LoadScene("Tutorial");
                break;
            case 1:
                SaveSystem.LoadGame();
                break;
            case 2:
                Application.OpenURL(Application.persistentDataPath);
                Application.OpenURL("https://www.surveymonkey.co.uk/r/GH9XGW2");
                Application.Quit();
                break;

        }

    }

}
