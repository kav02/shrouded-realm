﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BarScript : MonoBehaviour
{
    private GameObject Player;
    private Stats playerStats;
    Image HealthBar;
    Image MagixBar;

    // Start is called before the first frame update
    void Start()
    {
        Player = PlayerManager.instance.Player;
        playerStats = Player.GetComponent<Stats>();
        HealthBar = this.transform.Find("HealthBar").GetComponent<Image>();
        MagixBar = this.transform.Find("MagixBar").GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        float HealthFraction = (float)playerStats.Health / (float)playerStats.MaxHealth;
        float MagixFraction = (float)playerStats.Magix / (float)playerStats.MaxMagix;
        HealthBar.fillAmount = HealthFraction;
        MagixBar.fillAmount = MagixFraction;
    }
}
