﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ShowHide : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public GameObject Menu;

    private void HideAllMenusPotionShop()
    {
        this.transform.Find("InventoryBackground").gameObject.SetActive(false);
        this.transform.Find("MagixBackground").gameObject.SetActive(false);
        this.transform.Find("QuestsPage").gameObject.SetActive(false);
        this.transform.Find("LevelsBackground").gameObject.SetActive(false);
        this.transform.Find("SettingsBackground").gameObject.SetActive(false);
    }

    public void GeneralStoreEnable()
    {
        foreach (Transform child in this.transform)
        {
            if (child.name == "InventoryBackground" | child.name == "GeneralShop" | child.name == "Output")
            {
                child.gameObject.SetActive(true);
            }
            else
            {
                child.gameObject.SetActive(false);
            }
        }
    }

    public void GeneralStoreDisable()
    {
        foreach (Transform child in this.transform)
        {
            if (child.name == "GeneralShop" | child.name == "Descriptor" | child.name == "DeleteItem" | child.name == "PotionShop")
            {
                child.gameObject.SetActive(false);
            }
            else
            {
                child.gameObject.SetActive(true);
            }
        }
        Menu.GetComponent<ShopKeeper>().isGeneralStore = false;
        HideAllMenusPotionShop();
    }

    private void HideAllMenus()
    {
        this.transform.parent.Find("InventoryBackground").gameObject.SetActive(false);
        this.transform.parent.Find("MagixBackground").gameObject.SetActive(false);
        this.transform.parent.Find("QuestsPage").gameObject.SetActive(false);
        this.transform.parent.Find("LevelsBackground").gameObject.SetActive(false);
        this.transform.parent.Find("SettingsBackground").gameObject.SetActive(false);
    }

    public void PotionShopEnable()
    {
        foreach (Transform child in this.transform)
        {
            if(child.name == "InventoryBackground" | child.name == "PotionShop" | child.name == "Output")
            {
                child.gameObject.SetActive(true);
            }
            else
            {
                child.gameObject.SetActive(false);
            }
        }
    }

    public void PotionShopDisable()
    {
        foreach (Transform child in this.transform)
        {
            if (child.name == "PotionShop" | child.name == "Descriptor" | child.name == "DeleteItem" | child.name == "GeneralShop")
            {
                child.gameObject.SetActive(false);
            }
            else
            {
                child.gameObject.SetActive(true);
            }
        }
        Menu.GetComponent<PotionSeller>().isPotionShop = false;
        HideAllMenusPotionShop();
    }

    public void Toggle()
    {

        bool Activity; 
        if (Menu.activeSelf)
        {
            Activity = false;
        }
        else
        {
            Activity = true;
        }
        HideAllMenus();
        Menu.SetActive(Activity);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if(this.transform.Find("Icon") != null)
        {
            this.transform.Find("Icon").transform.position = this.transform.Find("Icon").transform.position + new Vector3(0, 10, 0);
        }

    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if(this.transform.Find("Icon") != null)
        {
            this.transform.Find("Icon").transform.position = this.transform.Find("Icon").transform.position - new Vector3(0, 10, 0);
        }
    }
}
