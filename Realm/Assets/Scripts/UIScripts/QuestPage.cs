﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class QuestPage : MonoBehaviour
{
    private int currentQuestIndex = 0;
    public List<Quest> Quests;
    private GameObject QuestHeader, QuestDescription;
    private GameObject btnNext, btnPrevious;
    public void Start()
    {
        QuestHeader = this.transform.Find("QuestInfo").Find("QuestTitle").gameObject;
        QuestDescription = this.transform.Find("QuestInfo").Find("QuestDescriptor").gameObject;
        btnNext = this.transform.Find("Next").gameObject;
        btnPrevious = this.transform.Find("Previous").gameObject;

    }

    public void Next()
    {
        Debug.Log(currentQuestIndex);
        currentQuestIndex = currentQuestIndex + 1;
        UpdateUI();
    }

    public void Previous()
    {
        
        currentQuestIndex = currentQuestIndex - 1;
        UpdateUI();
    }


    public void UpdateList(List<Quest> LoadedQuests)
    {
        Quests = LoadedQuests;

        UpdateUI();
    }

    public void UpdateUI()
    {
        if(Quests.Count != 0)
        {
            string HasCompleted;
            QuestHeader.GetComponent<TextMeshProUGUI>().text = Quests[currentQuestIndex].Title;
            if(Quests[currentQuestIndex].isCompleted == true)
            {
                HasCompleted = "Completed.";
            }
            else
            {
                HasCompleted = "In Progress..";
            }
            QuestDescription.GetComponent<TextMeshProUGUI>().text = Quests[currentQuestIndex].Description + "\n \n Progress: " + HasCompleted;
        } else
        {
            QuestHeader.GetComponent<TextMeshProUGUI>().text = "You have not started any quests...";
            QuestDescription.GetComponent<TextMeshProUGUI>().text = "People with an '!' above their head can assign you a quest.";
        }


        //Debug.Log("Current Quest Index: " + currentQuestIndex);
        //Debug.Log("Array max: " + Quests.Count);


        if (currentQuestIndex == 0)
        {
            btnPrevious.SetActive(false);
        }
        else
        {
            btnPrevious.SetActive(true);
        }

        if (currentQuestIndex == Quests.Count - 1 | Quests.Count == 0)
        {
            
            btnNext.SetActive(false);
        } else
        {
            btnNext.SetActive(true);
        }
    }
}
