﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GUIManager : MonoBehaviour
{
    #region Singleton
    public static GUIManager instance;

    private void Awake()
    {
        instance = this;
    }

    #endregion

    public GameObject UICanvas;
}
