﻿using UnityEngine;
using UnityEngine.UI;

public class NameTag : MonoBehaviour
{
    // Update is called once per frame
    private Text Nametext;
    public int Distance = 10;
    private Camera cam;

    private void Start()
    {
        GameObject QuestIcon = null;
        GameObject ChatIcon = null;
        GameObject PickupIcon = null;
        GameObject PotionMarker = null;

        if (this.transform.Find("QuestMarker")){ 
            QuestIcon = this.transform.Find("QuestMarker").gameObject;
        }
        if (this.transform.Find("ChatMarker"))
        {
            ChatIcon = this.transform.Find("ChatMarker").gameObject;
        }
        if (this.transform.Find("PickupMarker"))
        {
            PickupIcon = this.transform.Find("PickupMarker").gameObject;
        }
        if (this.transform.Find("PotionSellerMarker"))
        {
            PotionMarker = this.transform.Find("PotionSellerMarker").gameObject;
        }

        if (this.transform.parent.tag != "Pickup")
        {
            if (this.transform.Find("QuestMarker") != null && this.transform.Find("ChatMarker") != null && this.transform.Find("PotionSellerMarker") != null)
            {
                Destroy(PickupIcon);

                if (!this.transform.parent.GetComponent<ChatScript>())
                {
                    Destroy(ChatIcon);
                }

                if (!this.transform.parent.GetComponent<PotionSeller>())
                {
                    Destroy(PotionMarker);
                }

                if (!this.transform.parent.GetComponent<Quest>())
                {
                    Destroy(QuestIcon);
                }
            }
        }
        else
        {
            Destroy(ChatIcon);
            Destroy(QuestIcon);
            Destroy(PotionMarker);
        }
        
    
        if (this.transform.Find("Name") != null)
        {
            Nametext = this.transform.Find("Name").GetComponent<Text>();
            Nametext.text = this.transform.parent.transform.name;
            cam = Camera.main;
        }
    }

    void Update()
    {
        if (cam){


            //Debug.Log(Vector3.Distance(cam.transform.position, this.transform.position));

            transform.LookAt(transform.position + cam.transform.rotation * Vector3.forward, cam.transform.rotation * Vector3.up);
            if (Vector3.Distance(cam.transform.position, this.transform.position) > Distance)
            {
                foreach (Transform child in transform)
                {
                    child.gameObject.SetActive(false);
                }
            }
            else
            {
                foreach (Transform child in transform)
                {
                    child.gameObject.SetActive(true);
                }
            }
        }
    }
}
