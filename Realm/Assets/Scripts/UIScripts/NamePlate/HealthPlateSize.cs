﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthPlateSize : MonoBehaviour
{
    Image healthplate;
    Stats mobstats;


    void Start()
    {
        mobstats = this.transform.parent.GetComponent<Stats>();
        healthplate = this.transform.Find("HealthbarBackground").Find("Health").GetComponent<Image>();
        
    }

    // Update is called once per frame
    void Update()
    {
        float HealthFraction = (float)mobstats.Health / (float)mobstats.MaxHealth;
        healthplate.fillAmount = HealthFraction;
    }
}
