﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public static class OutputGUI
{
    static TextMeshProUGUI outputText;
    static Animator outputAnim;

    

    // Update is called once per frame
    public static void ShowPlayer(string Text)
    { 
        outputAnim = GUIManager.instance.UICanvas.transform.Find("GameUI").Find("Output").GetComponent<Animator>();
        outputText = GUIManager.instance.UICanvas.transform.Find("GameUI").Find("Output").Find("OutputText").GetComponent<TextMeshProUGUI>();


        outputAnim.Play("FadingOutput", -1, 0);
        outputText.text = Text;
        outputAnim.SetBool("IsFading", true);
    }
}
