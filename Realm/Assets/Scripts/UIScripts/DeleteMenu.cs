﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DeleteMenu : MonoBehaviour
{
    private string objectToDelete;
    private int Quantity;
    private InventorySystem plInventory;

    private void Start()
    {
        plInventory = PlayerManager.instance.Player.transform.Find("Inventory").GetComponent<InventorySystem>();
    }

    public void UpdateMenu(string PassedObjectName, int qty)
    {
        objectToDelete = PassedObjectName;
        this.transform.Find("Output").GetComponent<TextMeshProUGUI>().text = "you are about to delete " + PassedObjectName + " from your inventory.";
        Quantity = qty;
    }
    
    public void DeleteOne()
    {
        plInventory.TakeFromPlayer(objectToDelete, 1);
        Close();
    }
    
    public void DeleteAll()
    {
        plInventory.TakeFromPlayer(objectToDelete, Quantity);
        Close();
    }

    public void Close()
    {
        objectToDelete = null;
        Quantity = 0;
        this.gameObject.SetActive(false);
    }
}
