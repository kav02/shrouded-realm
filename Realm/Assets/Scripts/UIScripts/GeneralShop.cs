﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using TMPro;
using UnityEngine;

public class GeneralShop : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public int price;
    public string item;
    private GameObject Descriptor;

    private GameObject Player;

    private void Start()
    {
        Player = PlayerManager.instance.Player;
        Descriptor = GUIManager.instance.UICanvas.transform.Find("GameUI").transform.Find("Descriptor").gameObject;
    }
    public void Sell()
    {
        if (Player.transform.Find("Inventory").GetComponent<InventorySystem>().SearchForItem(item) >= 1)
        {
            Player.transform.Find("Inventory").GetComponent<InventorySystem>().TakeFromPlayer(item, 1);
            Player.GetComponent<PlayerLevels>().GiveGold(price);
            OutputGUI.ShowPlayer("You sold " + item + " for " + price + " realm gold.");
        }
        else
        {
            OutputGUI.ShowPlayer("You don't have any " + item + " to sell");
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        TextMeshProUGUI Text = Descriptor.transform.Find("Textbox").GetComponent<TextMeshProUGUI>();
        TextMeshProUGUI Title = Descriptor.transform.Find("Titlebox").GetComponent<TextMeshProUGUI>();
        RectTransform DescriptorTrans = Descriptor.GetComponent<RectTransform>();

        Title.text = item;
        Title.color = Color.black;
        Text.text = "Click to sell a " + item + ", you will get " + price + ".";



        DescriptorTrans.position = Input.mousePosition - new Vector3((DescriptorTrans.sizeDelta.x) / 1.8f, 0, 0);
        Descriptor.SetActive(true);

    }

    public void OnPointerExit(PointerEventData eventData)
    {
        Descriptor.SetActive(false);
    }
}