﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class LevelsUI : MonoBehaviour
{
    private GameObject UIMiningText;
    private GameObject UIMagixText;
    private GameObject UICombatText;
    private GameObject UIWoodcuttingText;
    private GameObject UIExplorationText;

    private PlayerLevels pLevels;

    public void Start()
    {
        UIMiningText = this.gameObject.transform.Find("MiningLvl").gameObject;
        UIMagixText = this.gameObject.transform.Find("MagixLvl").gameObject;
        UICombatText = this.gameObject.transform.Find("CombatLvl").gameObject;
        UIWoodcuttingText = this.gameObject.transform.Find("WoodcuttingLvl").gameObject;
        UIExplorationText = this.gameObject.transform.Find("ExplorationLvl").gameObject;
        pLevels = PlayerManager.instance.Player.GetComponent<PlayerLevels>();
        UpdateUI();
    }

    void ChangeText(GameObject UIText, string Text)
    {
        UIText.GetComponent<TextMeshProUGUI>().text = Text;
    }

    public void UpdateUI()
    {
        ChangeText(UIMiningText, "Lvl: " + pLevels.MiningLvl);
        ChangeText(UIMagixText, "Lvl: " + pLevels.MagixLvl);
        ChangeText(UICombatText, "Lvl: " + pLevels.CombatLvl);
        ChangeText(UIWoodcuttingText, "Lvl: " + pLevels.TreecuttingLvl);
        ChangeText(UIExplorationText, "Lvl: " + pLevels.ExplorationLvl);
    }
}
