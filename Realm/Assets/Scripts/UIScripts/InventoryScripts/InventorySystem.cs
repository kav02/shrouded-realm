﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventorySystem : MonoBehaviour
{
    public int[] QTYInventoryArr;
    public string[] ITInventoryArr;
    private GameObject plInventoryUI;
    public GameObject Hand;

    // Start is called before the first frame update
    public void Start()
    {
        plInventoryUI = GUIManager.instance.UICanvas.transform.Find("GameUI").transform.Find("InventoryBackground").gameObject;
        QTYInventoryArr = new int[12];
        ITInventoryArr = new string[12];

        UpdateImages();
    }

   public bool TakeFromPlayer(string Item, int Quantity)
    {
        bool SufficientAmount = false;
        for (int index = 0; index < ITInventoryArr.Length; index++)
        {
            if (ITInventoryArr[index] == Item)
            {
                if (QTYInventoryArr[index] == Quantity)
                {
                    Debug.Log("1");
                    QTYInventoryArr[index] = 0;
                    ITInventoryArr[index] = null;
                    UpdateImages();
                    SufficientAmount = true;
                } else if(QTYInventoryArr[index] > Quantity)
                {
                    Debug.Log("2");
                    QTYInventoryArr[index] = QTYInventoryArr[index] - Quantity;
                    UpdateImages();
                    SufficientAmount = true;
                }
                else
                {
                    Debug.Log("3");
                    Debug.Log("Player has item, but not enough of it.");
                    OutputGUI.ShowPlayer("you had some.. but not enough!");
                    SufficientAmount = false;
                }
                
            }        
        }

        return SufficientAmount;
    }

    public void Drop(int Index)
    {
        if (Index != -1)
        {
            if (QTYInventoryArr[Index] > 1)
            {
                QTYInventoryArr[Index] = QTYInventoryArr[Index] - 1;
                UpdateImages();
            }
            else if (QTYInventoryArr[Index] == 1)
            {
                QTYInventoryArr[Index] = 0;
                ITInventoryArr[Index] = null;
                UpdateImages();
            }
            else
            {
                //This shouldn't trigger..
                Debug.Log("Drop Function in inventory system script has fired and shouldn't.");
            }
        }

    }

    public bool Pickup(string Item, GameObject ItemRef, string Override)
    {
        bool PickedUp = false;
        for (int index = 0; index < ITInventoryArr.Length; index++)
        {
            if (!PickedUp)
            {
                if (ITInventoryArr[index] == Item)
                {
                    QTYInventoryArr[index] = QTYInventoryArr[index] + 1;
                    Destroy(ItemRef);
                    PickedUp = true;
                    UpdateImages();
                    Debug.Log("Item already in inventory, added one to quantity.");
                }
            }
        }

        for (int index = 0; index < ITInventoryArr.Length; index++)
        {
            if (string.IsNullOrEmpty(ITInventoryArr[index]) && !PickedUp)
            {
                ITInventoryArr[index] = Item;
                QTYInventoryArr[index] = 1;
                Destroy(ItemRef);
                PickedUp = true;
                UpdateImages();
                Debug.Log("New item in inventory.");
            }
    }

        if (!PickedUp)
        {
            Debug.Log("Inventory full!");
            OutputGUI.ShowPlayer("Inventory is full.");
            PickedUp = false;
        } else if (PickedUp & Override == null)
        {
            OutputGUI.ShowPlayer("You picked up " + Item);
        }
        else
        {
            OutputGUI.ShowPlayer(Override);
        }

        return PickedUp;
    }

    public void UpdateImages()
    {
        for(int i = 0; i < plInventoryUI.transform.childCount; i++)
        {
            if (plInventoryUI.transform.GetChild(i).transform.name != "RealmGold")
            {
                if (!(string.IsNullOrEmpty(ITInventoryArr[i])))
                {
                    plInventoryUI.transform.GetChild(i).GetComponent<Image>().sprite = Resources.Load<Sprite>(ITInventoryArr[i]);
                    plInventoryUI.transform.GetChild(i).transform.Find("Quantity").GetComponent<Text>().text = ("x " + QTYInventoryArr[i].ToString());
                }
                else if (string.IsNullOrEmpty(ITInventoryArr[i]))
                {
                    plInventoryUI.transform.GetChild(i).GetComponent<Image>().sprite = Resources.Load<Sprite>("Blank");
                    plInventoryUI.transform.GetChild(i).transform.Find("Quantity").GetComponent<Text>().text = null;
                    //   Debug.Log("Blanked");
                }
            }
        }

        //This way is deprecated, the new way updates all tiles rather than specific tiles.
        //GameObject tile = plInventoryUI.transform.GetChild(Index).gameObject;
        //tile.GetComponent<Image>().sprite = Resources.Load<Sprite>(ItemName);
    }

    public void attachItemToHand(string ItemName, bool canSwing, int Index)
    {
        GameObject Player = PlayerManager.instance.Player;
      
        if (!string.IsNullOrEmpty(Player.GetComponent<PlayerMovement>().currentWield))
        {
            Debug.Log("Player is already wielding a weapon.");
            OutputGUI.ShowPlayer("you were already holding a weapon, changing over..");
            Player.GetComponent<PlayerMovement>().Unwield(null);
        }

        GameObject item = Resources.Load<GameObject>("Tools/" + ItemName);
        if (item != null)
        {
            GameObject copy = Instantiate(item, Hand.transform);
            ToolPosition properties = copy.GetComponent<ToolPosition>();
            copy.transform.localPosition = properties.Pos;
            copy.transform.localEulerAngles = properties.Rot;
            copy.transform.localScale = properties.Sca;
            copy.transform.name = ItemName;

            Player.GetComponent<PlayerMovement>().Wielded = true;
            Player.GetComponent<PlayerMovement>().CanSwing = canSwing;
            Player.GetComponent<PlayerMovement>().currentWield = ItemName;

            Drop(Index);
        }
        else
        {
            Debug.Log("File does not exist in resources.");
            OutputGUI.ShowPlayer("you cannot equip this.");
        }


    }

    public int SearchForItem(string ItemName)
    {
        int returnvalue = 0;

        for(int index = 0; index < ITInventoryArr.Length; index++)
        {
            if(ITInventoryArr[index] == ItemName)
            {
                returnvalue = QTYInventoryArr[index];
            }
        }
        if (PlayerManager.instance.Player.GetComponent<PlayerMovement>().currentWield == ItemName)
        {
            returnvalue++;
        }

        return returnvalue;
    }

    public void ClearInventory()
    {
        
        PlayerManager.instance.Player.GetComponent<PlayerMovement>().UseItem();
        for (int index = 0; index < ITInventoryArr.Length; index++)
        {
            ITInventoryArr[index] = "";
            QTYInventoryArr[index] = 0;
        }
        
    }

    private void OnTriggerEnter(Collider collision)
    {

        if (collision.gameObject.tag == "Pickup")
        {
            Debug.Log("Player has collided with a pickup" + collision.gameObject.name);
            Pickup(collision.gameObject.name, collision.gameObject, null);
        }
    }

}
