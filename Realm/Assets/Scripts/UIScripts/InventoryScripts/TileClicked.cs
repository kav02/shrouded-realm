﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;
using System.IO;
using TMPro;

public class TileClicked : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
{
    public int Index = 0;
    private GameObject Inventory;
    public GameObject Hand;
    private GameObject Player;
    private GameObject Descriptor;

    private void Start()
    {
        Player = PlayerManager.instance.Player;
        Inventory = Player.transform.Find("Inventory").gameObject;
        Descriptor = GUIManager.instance.UICanvas.transform.Find("GameUI").transform.Find("Descriptor").gameObject;
    }

    /*void attachItemToHand(string ItemName, bool canSwing) //moved to inventorysystem.
    {
        if (!string.IsNullOrEmpty(Player.GetComponent<PlayerMovement>().currentWield))
        {
            Debug.Log("Player is already wielding a weapon.");
            OutputGUI.ShowPlayer("you were already holding a weapon, changing over..");
            Player.GetComponent<PlayerMovement>().Unwield();
        }

        GameObject item = Resources.Load<GameObject>("Tools/" + ItemName);
        if (item != null)
        {
            GameObject copy = Instantiate(item, Hand.transform);
            ToolPosition properties = copy.GetComponent<ToolPosition>();
            copy.transform.localPosition = properties.Pos;
            copy.transform.localEulerAngles = properties.Rot;
            copy.transform.localScale = properties.Sca;
            copy.transform.name = ItemName;

            Player.GetComponent<PlayerMovement>().Wielded = true;
            Player.GetComponent<PlayerMovement>().CanSwing = canSwing;
            Player.GetComponent<PlayerMovement>().currentWield = ItemName;

            Inventory.GetComponent<InventorySystem>().Drop(Index);
        }
        else
        {
            Debug.Log("File does not exist in resources.");
            OutputGUI.ShowPlayer("you cannot equip this.");
        }
    }
    */

    public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Left)
        {
            string item = Inventory.GetComponent<InventorySystem>().ITInventoryArr[Index];

            OutputGUI.ShowPlayer("Player has equipped " + item);
            Debug.Log("Player has equipped " + item);
            Inventory.GetComponent<InventorySystem>().attachItemToHand(item, true, Index);
        }
        else if(eventData.button == PointerEventData.InputButton.Right)
        {
            string item = Inventory.GetComponent<InventorySystem>().ITInventoryArr[Index];
            int quantity = Inventory.GetComponent<InventorySystem>().QTYInventoryArr[Index];
            if(quantity != 0)
            {
                DeleteMenu mDelete = GUIManager.instance.UICanvas.transform.Find("GameUI").Find("DeleteItem").GetComponent<DeleteMenu>();
                mDelete.gameObject.SetActive(true);
                mDelete.UpdateMenu(item, quantity);
            }
        }
    }


    public void OnPointerEnter(PointerEventData eventData)
    {
        TextMeshProUGUI Text = Descriptor.transform.Find("Textbox").GetComponent<TextMeshProUGUI>();
        TextMeshProUGUI Title = Descriptor.transform.Find("Titlebox").GetComponent<TextMeshProUGUI>();
        RectTransform DescriptorTrans = Descriptor.GetComponent<RectTransform>();

        if (Inventory.GetComponent<InventorySystem>().QTYInventoryArr[Index] != 0)
        {
            Debug.Log("Mouse is over");
            Title.text = Inventory.GetComponent<InventorySystem>().ITInventoryArr[Index];
            Title.color = Color.black;
            Text.text = "An item in your inventory.";

        }
        else
        {
            Title.color = new Color(0, 0, 0);
            Title.text = "Empty Slot";
            Text.text = "This is an empty space in your inventory";
        }

        DescriptorTrans.position = Input.mousePosition - new Vector3((DescriptorTrans.sizeDelta.x) / 1.8f, 0, 0);
        Descriptor.SetActive(true);

    }

    public void OnPointerExit(PointerEventData eventData)
    {
        Descriptor.SetActive(false);
    }

    /*public void Clicked()
    {
        string item = Inventory.GetComponent<InventorySystem>().ITInventoryArr[Index];

            Debug.Log("Player has equipped " + item);
            attachItemToHand(item, true);
        
            string item = Inventory.GetComponent<InventorySystem>().ITInventoryArr[Index];
        switch (item)
        {
            case "Sword":
                Debug.Log("Player has equipped sword");
                attachItemToHand(item, true);
                break;
            case "Pickaxe":
                Debug.Log("Player has equipped pickaxe");
                attachItemToHand(item, true);
                break;
            default:
                Debug.Log("Item is unusable");
                break;

        }

        */

}
