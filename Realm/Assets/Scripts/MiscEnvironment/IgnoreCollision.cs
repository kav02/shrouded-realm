﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IgnoreCollision : MonoBehaviour
{
    private new Collider collider;
    private void Start()
    {
        collider = this.GetComponent<Collider>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.layer != 8)
        {
            Physics.IgnoreCollision(collision.gameObject.GetComponent<Collider>(), collider);
        }
    }
}
