﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchMessage : MonoBehaviour
{
    public string Message;
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            OutputGUI.ShowPlayer(Message);
        }
    }
}
