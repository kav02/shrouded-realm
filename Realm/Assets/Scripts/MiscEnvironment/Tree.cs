﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tree : MonoBehaviour
{
    public int LevelReq = 0;
    public int Health = 100;
    public int Experience = 10;


    public void Bash(int damage)
    {
        if (Health-damage <= 0)
        {
            Die();
        }
        else {
            Health = Health - damage;
        }
    }

    void Die()
    {
        PlayerManager.instance.Player.GetComponent<PlayerLevels>().AddTreecutting(Experience);

        if (LevelReq >= 10)
        {
            PlayerManager.instance.Player.transform.Find("Inventory").GetComponent<InventorySystem>().Pickup("GreatWood", null, null);
        }
        else
        {
            PlayerManager.instance.Player.transform.Find("Inventory").GetComponent<InventorySystem>().Pickup("MapleWood", null, null);
        }
        this.gameObject.SetActive(false);
    }
}
