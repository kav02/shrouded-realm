﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideObject : MonoBehaviour
{
    private string ItemToHideName;
    public int QuantityOfItem;

    // Start is called before the first frame update
    void Start()
    {
        ItemToHideName = this.transform.name;
        StartCoroutine(AfterGameInitiation());
    }

    IEnumerator AfterGameInitiation()
    {
        yield return new WaitForSeconds(.5f);
        if (PlayerManager.instance.Player.transform.Find("Inventory").GetComponent<InventorySystem>().SearchForItem(ItemToHideName) >= QuantityOfItem)
        {
            Debug.Log("Found");
            Destroy(this.gameObject);
        }
    }
}
