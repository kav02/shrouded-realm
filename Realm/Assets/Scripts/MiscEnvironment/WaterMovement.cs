﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterMovement : MonoBehaviour
{
    public float Speed = 5f;
    // Start is called before the first frame update
    void Update()
    {
        float rot = Speed * Time.deltaTime;
        transform.Rotate(0, rot, 0, Space.World);
    }
}
