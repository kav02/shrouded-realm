﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LunaLostIt : MonoBehaviour
{
    public GameObject LunaTeleport;
    public GameObject CameraTeleport;
    public GameObject Particles;
    public GameObject Gemma;
    private Animator anim;

    // Start is called before the first frame update
    void Start()
    {
        GameObject Luna = this.transform.parent.gameObject;
        GameObject Cam = Luna.transform.Find("Camera").gameObject;
        GameObject NameTag = Luna.transform.Find("NameTag").gameObject;
        GameObject Floating = this.transform.parent.parent.gameObject;
        RectTransform NametagTransform = NameTag.GetComponent<RectTransform>();
        NametagTransform.localPosition = new Vector3(0.021f, 0.607f, 0.136f);
        anim = Luna.GetComponent<Animator>();
        Luna.transform.position = LunaTeleport.transform.position;
        Luna.transform.rotation = LunaTeleport.transform.rotation;
        Luna.transform.parent = null;
        anim.SetBool("isLeaning", true);
        Cam.transform.position = CameraTeleport.transform.position;
        Cam.transform.rotation = CameraTeleport.transform.rotation;
        Destroy(Floating);
        Destroy(Particles);
        Gemma.GetComponent<ChatScript>().chat[0] = "Welldone for saving that floating person!";
        Gemma.GetComponent<ChatScript>().chat[1] = "I would have helped save her if she didn't startle us.";

    }
}
