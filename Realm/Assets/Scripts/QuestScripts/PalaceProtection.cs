﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PalaceProtection : MonoBehaviour
{
    public GameObject Bubble;
    public GameObject Michonne;
    private float alpha = 0f;
    // Start is called before the first frame update
    void Start()
    {
        Bubble.SetActive(true);
        StartCoroutine(FadeIn());
        Michonne.GetComponent<ChatScript>().chat[1] = "Thanks for protecting the palace!";
    }

    IEnumerator FadeIn()
    {
        Bubble.GetComponent<MeshRenderer>().material.color = new Color(0.9720305f, 0.7783019f, 1f, alpha);
        alpha = alpha + 0.01f;
        yield return new WaitForSeconds(.03f);
        if (alpha <= 0.5f) {
            StartCoroutine(FadeIn());
        }
    }
}
