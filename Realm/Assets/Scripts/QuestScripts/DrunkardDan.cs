﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrunkardDan : MonoBehaviour
{
    public GameObject Milkbucket;
    public GameObject MilkPickup;
    public GameObject Paul;

    // Start is called before the first frame update
    void Start()
    {
        Milkbucket.SetActive(true);
        Paul.GetComponent<ChatScript>().chat[1] = "Did you take the bucket of milk that was on the floor?!";
        if (MilkPickup)
        {
            Destroy(MilkPickup);
        }
    }
}
