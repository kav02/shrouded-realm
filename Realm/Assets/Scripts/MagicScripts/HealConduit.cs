﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealConduit : MonoBehaviour
{
    private GameObject HealingFX;
    private GameObject HealLine;
    private GameObject Player;
    void Start()
    {
        GameObject HealingFXpref = Resources.Load<GameObject>("FX/" + "HealingFX");
        GameObject HealLinepref = Resources.Load<GameObject>("FX/" + "HealLineFX");
        Player = PlayerManager.instance.Player;
        HealingFX = Instantiate(HealingFXpref, Player.transform);
        HealLine = Instantiate(HealLinepref);
        Player.GetComponent<PlayerLevels>().AddMagix(80);
        StartCoroutine(BeginFX());
    }

    // Update is called once per frame
    void Update()
    {
        LineRenderer Line = HealLine.GetComponent<LineRenderer>();
        Line.SetPosition(0, this.transform.position);
        Line.SetPosition(1, (Player.transform.position + new Vector3(0, 1, 0)));
        if (Vector3.Distance(this.transform.position, Player.transform.position) < 10)
        {
            HealLine.SetActive(true);
            HealingFX.SetActive(true);
            Player.GetComponent<Stats>().Healstep = 15;
        }
        else
        {
            HealLine.SetActive(false);
            HealingFX.SetActive(false);
            Player.GetComponent<Stats>().Healstep = 1;
        }
    }

    IEnumerator BeginFX()
    {

        yield return new WaitForSeconds(10);
        Destroy(HealLine);
        Destroy(HealingFX);
        Player.GetComponent<Stats>().Healstep = 1;
        Destroy(this.gameObject);
    }
}
