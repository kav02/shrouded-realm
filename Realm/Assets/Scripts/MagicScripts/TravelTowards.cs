﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TravelTowards : MonoBehaviour
{

    public Vector3 Target;
    private Rigidbody m_Rigidbody;
    public float m_Speed = 10f;
    public int Damage = 30;
    public bool FreezeEnemy = false;
    public int XPReward = 30;

    private GameObject p_body;
    private GameObject p_explosion;

    private bool canHit = true;

    // Start is called before the first frame update
    void Start()
    {
        m_Rigidbody = this.GetComponent<Rigidbody>();
        p_body = this.transform.Find("Body").gameObject;
        p_explosion = this.transform.Find("Explosion").gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.LookAt(Target);
        m_Rigidbody.velocity = transform.forward * m_Speed;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag != "Player")
        {
            if (other.tag == "Hostile" && canHit)
            {
                canHit = false;
                Stats EnemyStats = other.GetComponent<Stats>();
                if (FreezeEnemy)
                {

                    other.gameObject.AddComponent<Freeze>();
                }
                if (EnemyStats.Health - Damage <= 0)
                {
                    PlayerManager.instance.Player.GetComponent<PlayerLevels>().AddMagix(XPReward);
                    Debug.Log("Player has killed " + other.name + " with magic.");
                    OutputGUI.ShowPlayer("you have killed " + other.name + "with magic.");
                }
                other.GetComponent<Stats>().Health = other.GetComponent<Stats>().Health - Damage;
            }
            StartCoroutine(Hit());
        }
    }

    IEnumerator Hit()
    {
        m_Speed = 0;
        p_body.SetActive(false);
        p_explosion.SetActive(true);
        yield return new WaitForSeconds(1);
        Destroy(this.gameObject);
    }
}
