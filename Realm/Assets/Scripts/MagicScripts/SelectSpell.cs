﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;// Required when using Event data

public class SelectSpell : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public int MinLevel = 1;
    public string SpellName;
    public string Description;
    public Sprite Icon;
    public Color TextColor;
    public int Cost = 10;

    private GameObject Descriptor;
    private bool Known = false;

    void Start()
    {
        Descriptor = GUIManager.instance.UICanvas.transform.Find("GameUI").transform.Find("Descriptor").gameObject;
    }


    public void UpdateUI(int MagixLevel)
    {
        if(MagixLevel >= MinLevel)
        {
            this.GetComponent<Image>().sprite = Icon;
            Known = true;
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        TextMeshProUGUI Text = Descriptor.transform.Find("Textbox").GetComponent<TextMeshProUGUI>();
        TextMeshProUGUI Title = Descriptor.transform.Find("Titlebox").GetComponent<TextMeshProUGUI>();
        RectTransform DescriptorTrans = Descriptor.GetComponent<RectTransform>();

        if (Known)
        {
            Debug.Log("Mouse is over");
            Title.text = SpellName;
            Title.color = TextColor;
            Text.text = Description;

        }
        else
        {
            Title.color = new Color(0, 0, 0);
            Title.text = "?";
            Text.text = "You lack the knowledge to cast this spell... Unlock at Magix level " + MinLevel;
        }

        DescriptorTrans.position = Input.mousePosition - new Vector3((DescriptorTrans.sizeDelta.x) / 1.8f, 0, 0);
        Descriptor.SetActive(true);

    }

    public void OnPointerExit(PointerEventData eventData)
    {
        Descriptor.SetActive(false);
    }

    public void SetSpell()
    {
        if (Known)
        {
            PlayerManager.instance.Player.GetComponent<PlayerLevels>().Currentspell = SpellName;
            PlayerManager.instance.Player.GetComponent<PlayerLevels>().Cost = Cost;
        }
        else
        {
            Debug.Log("Player doesn't know this spell.");
            OutputGUI.ShowPlayer("you lack the knowledge to cast this spell.");
        }
        //change current spell icon too

    }

}
