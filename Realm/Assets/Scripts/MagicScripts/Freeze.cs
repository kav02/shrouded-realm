﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Freeze : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Stick());
    }

    IEnumerator Stick()
    {
        this.transform.Find("Humanoid").GetComponent<Animator>().speed = 0;
        this.GetComponent<NavMeshAgent>().enabled = false;
        this.GetComponent<EnemyController>().enabled = false;
        Debug.Log("Frozen " + this.name);
        OutputGUI.ShowPlayer("You froze " + this.name);
        yield return new WaitForSeconds(10);
        this.transform.Find("Humanoid").GetComponent<Animator>().speed = 1;
        this.GetComponent<NavMeshAgent>().enabled = true;
        this.GetComponent<EnemyController>().enabled = true;
    }

}
