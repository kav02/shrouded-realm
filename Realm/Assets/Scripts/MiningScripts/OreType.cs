﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OreType : MonoBehaviour
{
    public string TypeOfOre = "IronOre";
    public bool regen = true;
    public int EXP = 10;
    public int RequiredLevel = 5;
    public int MaxHealth = 100;
    private int Health;
    private GameObject Player;
    private bool isActive = true;

    private void Start()
    {
        Player = PlayerManager.instance.Player;
        Health = MaxHealth;
    }

    public void Damage(int Damage)
    {
        if (Player.GetComponent<PlayerLevels>().MiningLvl >= RequiredLevel)
        {
            if ((Health - Damage) <= 0 & isActive)
            {
                Health = 0;
                Debug.Log("Player has mined " + TypeOfOre + " ore.");
                OutputGUI.ShowPlayer("Player has mined " + TypeOfOre + " ore.");
                Player.GetComponent<PlayerLevels>().AddMining(EXP);
                Player.transform.Find("Inventory").GetComponent<InventorySystem>().Pickup(TypeOfOre, null, null);
                this.transform.Find("Rock").gameObject.SetActive(false);
                this.transform.Find("Ore").gameObject.SetActive(false);
                this.GetComponent<BoxCollider>().enabled = false;
                isActive = false;
                StartCoroutine(Regenerate());
            }
            else
            {
                if (isActive)
                {
                    Health = Health - Damage;
                }
                else
                {
                    Debug.Log("Player is attempting to hit an invisible ore");
                }
            }
        }
        else
        {
            Debug.Log("Player lacks level to mine this ore.");
            OutputGUI.ShowPlayer("This rock is too hard for you, level up your mining first.");
        }
    }

    IEnumerator Regenerate()
    {
        if (regen)
        {
            yield return new WaitForSeconds(10);
            this.transform.Find("Rock").gameObject.SetActive(true);
            this.transform.Find("Ore").gameObject.SetActive(true);
            this.GetComponent<BoxCollider>().enabled = true;
            isActive = true;
            Health = MaxHealth;
        }
    }
}
