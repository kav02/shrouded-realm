﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Stats : MonoBehaviour
{
    bool isRegenHealth = false;
    bool isRegenMagix = false;

    bool alive = true;
    public int MaxHealth, Health, MaxMagix, Magix, XPWorth, Healstep = 1;

    public void Damage(int value)
    {
        Health = Health - value;
    }

    public void Heal(int value)
    {
        if (Health + value > MaxHealth)
        {
            Health = MaxHealth;
        }
        else if (Health < MaxHealth)
        {
            Health = Health + value;
        }
    }

    public void RegenMagix(int value)
    {
        if (Magix + value > MaxMagix)
        {
            Magix = MaxMagix;
        }
        else if (Magix < MaxMagix)
        {
            Magix = Magix + value;
        }
    }

    public void SetTransform(Vector3 oldposition, Vector3 oldrotation)
    {
        this.GetComponent<PlayerMovement>().enabled = false;
        this.transform.position = oldposition;
        this.transform.rotation = Quaternion.Euler(oldrotation);
        this.GetComponent<PlayerMovement>().enabled = true;
    }

    private void Update()
    {
        if (Health <= 0 && alive && this.tag == "Player")
        {
            alive = false;
            Debug.Log("Player has died");
            OutputGUI.ShowPlayer("you died.");
            StartCoroutine(Death());
        }
        else if (Health != MaxHealth & isRegenHealth == false)
        {
            StartCoroutine(RegainHealthOverTime());
        }

        if (Magix != MaxMagix & isRegenMagix == false)
        {
            StartCoroutine(RegainMagixOverTime());
        }
    }

    IEnumerator RegainHealthOverTime()
    {
        isRegenHealth = true;
        while (Health < MaxHealth)
        {
            Heal(Healstep);
            yield return new WaitForSeconds(2);
        }
        isRegenHealth = false;
    }

    IEnumerator RegainMagixOverTime()
    {
        isRegenMagix = true;
        while (Magix < MaxMagix)
        {
            RegenMagix(Healstep);
            yield return new WaitForSeconds(.5f);
        }
        isRegenMagix = false;
    }
    IEnumerator Death()
    {
        GUIManager.instance.UICanvas.GetComponent<TransitionLayer>().FadeOut();
        this.GetComponent<Animator>().SetLayerWeight(3, 1);
        this.GetComponent<Animator>().SetBool("isDead", true);
        yield return new WaitForSeconds(2);
        SceneManager.LoadScene("DeathScene");
    }
}
