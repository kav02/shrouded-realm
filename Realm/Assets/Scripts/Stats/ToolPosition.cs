﻿
using UnityEngine;

public class ToolPosition : MonoBehaviour
{
    public Vector3 Pos;
    public Vector3 Rot;
    public Vector3 Sca;
    public bool Swinging = false;
    public bool canSwing = true;
}
