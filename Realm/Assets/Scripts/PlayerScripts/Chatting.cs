﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Chatting : MonoBehaviour
{
    private GameObject GameUI;
    private GameObject ChatUI;
    private int IndexOfChat;
    private string[] Chat;


    private Camera mainCam;
    private Camera tempCam;

    // Start is called before the first frame update
    void OnEnable()
    {
        GameUI = GUIManager.instance.UICanvas.transform.Find("GameUI").gameObject;
        ChatUI = GUIManager.instance.UICanvas.transform.Find("ChatUI").gameObject;
        this.gameObject.GetComponent<PlayerMovement>().enabled = false;
        InvisiblePlayer();
        GameUI.SetActive(false);
        ChatUI.SetActive(true);
        IndexOfChat = 0;
        mainCam = CameraManager.instance.Camera.GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        bool enabled = true;
        if (Input.GetMouseButtonDown(0) & enabled)
        {
            enabled = false;
            IndexOfChat = IndexOfChat + 1;
            UpdateChat(IndexOfChat);
        }
        if (Input.GetMouseButtonUp(0))
        {
            enabled = true;
        }
    }

    void UpdateChat(int Index)
    {
        if (Chat.Length <= IndexOfChat) {
            EndChat();
        }
        else
        {
            ChatUI.transform.Find("Page").transform.Find("Chat").GetComponent<Text>().text = Chat[Index];
        }
    }

    public void EndChat()
    {
        this.gameObject.GetComponent<PlayerMovement>().enabled = true;
        GameUI.SetActive(true);
        ChatUI.SetActive(false);
        this.enabled = false;
        tempCam.enabled = false;
        mainCam.enabled = true;
        VisiblePlayer();
    }



    public void StartChat(string[] monologue, int length, Camera newcam)
    {
        this.enabled = true;
        tempCam = newcam;

        if (!(newcam == null))
        {
            newcam.enabled = true;
            mainCam.enabled = false;
        }

        Chat = new string[length];
        Chat = monologue;
        UpdateChat(0);
    }

    private void InvisiblePlayer()
    {
        foreach (Transform child in this.transform)
        {
            if (child.GetComponent<SkinnedMeshRenderer>() != null)
            {
                child.GetComponent<SkinnedMeshRenderer>().enabled = false;
            }
        }
    }

    private void VisiblePlayer()
    {
        foreach (Transform child in this.transform)
        {
            if (child.GetComponent<SkinnedMeshRenderer>() != null)
            {
                child.GetComponent<SkinnedMeshRenderer>().enabled = true;
            }
        }
    }
}
