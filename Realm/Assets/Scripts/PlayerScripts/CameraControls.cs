﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControls : MonoBehaviour
{
    protected Transform CameraTransform;
    protected Transform ParentTransform;

    protected Vector3 _LocalRotation;
    protected float _CameraDistance = 10f;

    public float MouseSensitivity = 4f;
    public float ScrollSensitivity = 2f;
    public float OrbitDampening = 10f;
    public float ScrollDampening = 6f;

    public bool CameraDisabled = true;

    // Start is called before the first frame update
    void Start()
    {
        CameraTransform = this.transform;
        ParentTransform = this.transform.parent;
        
    }

    // Update is called once per frame


    void LateUpdate()
    {

        if (Input.GetMouseButtonDown(1)) {
            CameraDisabled = false;
        }
        if (Input.GetMouseButtonUp(1))
        {
            CameraDisabled = true;
        }

        if (!CameraDisabled)
        {
            //rotation
            if (Input.GetAxis("Mouse X") != 0 || Input.GetAxis("Mouse Y") != 0)
            {
                _LocalRotation.x += Input.GetAxis("Mouse X") * MouseSensitivity;
                _LocalRotation.y -= Input.GetAxis("Mouse Y") * MouseSensitivity;

                //clamping
                _LocalRotation.y = Mathf.Clamp(_LocalRotation.y, 0f, 90f);
            }



            //transform for rotation
            Quaternion QT = Quaternion.Euler(_LocalRotation.y, _LocalRotation.x, 0);
            this.ParentTransform.rotation = Quaternion.Lerp(this.ParentTransform.rotation, QT, Time.deltaTime * OrbitDampening);


        }
        //scrolling input
        if (Input.GetAxis("Mouse ScrollWheel") != 0)
        {
            float ScrollAmount = Input.GetAxis("Mouse ScrollWheel") * ScrollSensitivity;

            ScrollAmount *= (this._CameraDistance * 0.3f);

            this._CameraDistance += ScrollAmount * -1f;

            this._CameraDistance = Mathf.Clamp(this._CameraDistance, 1.5f, 100f);
        }
        //updates scrolling
        if (this.CameraTransform.localPosition.z != this._CameraDistance * -1f)
        {
            this.CameraTransform.localPosition = new Vector3(0f, 0f, Mathf.Lerp(this.CameraTransform.localPosition.z, this._CameraDistance * -1f, Time.deltaTime * ScrollDampening));
        }


    }
}
