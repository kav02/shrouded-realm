﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerLevels : MonoBehaviour
{
    [SerializeField]
    private int OverallLvl = 1;

    public bool isLoadGame = true;

    public int MagixLvl = 1;
    public int MagixXP = 0;
    public int CombatLvl = 1;
    public int CombatXP = 0;
    public int MiningLvl = 1;
    public int MiningXP = 0;
    public int TreecuttingLvl = 1;
    public int TreecuttingXP = 0;
    public int ExplorationLvl = 1;
    public int ExplorationXP = 0;

    public int RealmGold = 10;

    private LevelsUI lUI;
    private TextMeshProUGUI RealmGoldUI;

    public string Currentspell;
    public int Cost;

    private void Start()
    {
        InventorySystem Inventory = this.transform.Find("Inventory").GetComponent<InventorySystem>();
        Inventory.Start();
        UpdateMagixUI();
        Currentspell = "Fireball";
        Cost = 10;
        lUI = GUIManager.instance.UICanvas.transform.Find("GameUI").Find("LevelsBackground").Find("LevelsText").GetComponent<LevelsUI>();
        RealmGoldUI = GUIManager.instance.UICanvas.transform.Find("GameUI").Find("InventoryBackground").Find("RealmGold").Find("Value").GetComponent<TextMeshProUGUI>();
        UpdateGoldUI();
        lUI.Start();
        if (SaveSystem.LoadPlayer() != null && isLoadGame)
        {
            SaveSystem.LoadGame();
        }

    }

    public void UpdateGoldUI()
    {
        RealmGoldUI.text = "Realm Gold;" + RealmGold;
    }


    public int CalculateOverallLevel()
    {
        int CalculatedLevel = (int)(Mathf.Floor((CombatLvl + MagixLvl) / 2) + Mathf.Floor(MiningLvl / 3) + Mathf.Floor(ExplorationLvl / 5));
        OverallLvl = CalculatedLevel;
        return CalculatedLevel;
    }

    

    public void AddMagix(int XPAdd)
    {
        MagixXP = MagixXP + XPAdd;
        MagixLvl = GenerateLevel(MagixLvl, MagixXP);
        lUI.UpdateUI();
        UpdateMagixUI();

    }

    public void AddCombat(int XPAdd)
    {
        CombatXP = CombatXP + XPAdd;
        CombatLvl = GenerateLevel(CombatLvl, CombatXP);
        lUI.UpdateUI();
    }

    public void AddMining(int XPAdd)
    {
        MiningXP = MiningXP + XPAdd;
        MiningLvl = GenerateLevel(MiningLvl, MiningXP);
        lUI.UpdateUI();
    }

    public void AddTreecutting(int XPAdd)
    {
        TreecuttingXP = TreecuttingXP + XPAdd;
        TreecuttingLvl = GenerateLevel(TreecuttingLvl, TreecuttingXP);
        lUI.UpdateUI();
    }

    public void AddExploration(int XPAdd)
    {
        ExplorationXP = ExplorationXP + XPAdd;
        ExplorationLvl = GenerateLevel(ExplorationLvl, ExplorationXP);
        lUI.UpdateUI();
    }

    public void UpdateLevels()
    {
        MagixLvl = GenerateLevel(MagixLvl, MagixXP);
        CombatLvl = GenerateLevel(CombatLvl, CombatXP);
        MiningLvl = GenerateLevel(MiningLvl, MiningXP);
        TreecuttingLvl = GenerateLevel(TreecuttingLvl, TreecuttingXP);
        ExplorationLvl = GenerateLevel(ExplorationLvl, ExplorationXP);
        lUI.UpdateUI();
    }
    //used to calculate magix, combat etc.
    private int GenerateLevel(int currentLevel, int XP)
    {
        //equation used to define the level from XP.
        int newLevel = (int)(Mathf.Floor((Mathf.Sqrt(XP) / 2) + 1));


        if (newLevel> currentLevel)
        {
            //player is rewarded currency for levelling up.
            Debug.Log("Player has levelled up in a skill!");
            OutputGUI.ShowPlayer("You levelled up");
            RealmGold = RealmGold + newLevel;
        }

        return newLevel;
    }

    public void UpdateMagixUI()
    {
        //update the Magix menu, so that if the player unlocks any new spells they appear usable.
        foreach (Transform child in GUIManager.instance.UICanvas.transform.Find("GameUI").transform.Find("MagixBackground").transform.Find("Spells").transform)
        {
            child.GetComponent<SelectSpell>().UpdateUI(MagixLvl);
        }
    }

    public void GiveGold(int Raise)
    {
        RealmGold = RealmGold + Raise;
        UpdateGoldUI();
    }

    public bool Purchase(int price)
    {
        bool returnval = false;
        if((RealmGold - price) < 0)
        {
            returnval = false;
            OutputGUI.ShowPlayer("You don't have enough realm gold.");
        }
        else
        {
            RealmGold = RealmGold - price;
            returnval = true;
        }
        UpdateGoldUI();
        return returnval;
    }

}
