﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CollectSaveData
{
    public float rotation;
    public float[] position;
    public int Scene;
    //basic stats
    public int Health;
    public int Magix;

    //levels
    public int Combatxp;
    public int Magixxp;
    public int Miningxp;
    public int Treecuttingxp;
    public int Explorationxp;
    
    //Inventory
    public string[] ITInventory;
    public int[] QTYInventory;
    public string WieldedWeapon;
    public int Realmgold;

    //Quests
    public int NumOfQuests;
    public string[] QuestNames;
    public string[] QuestDescriptions;
    public bool[] QuestCompletions;



    public CollectSaveData(InventorySystem invent, Stats plstats, PlayerLevels plLevels, Vector3 plPos, List<Quest> Quests, int sceneindex, string Weapon, float Rotation)
    {

        position = new float[3];
        position[0] = plPos.x;
        position[1] = plPos.y;
        position[2] = plPos.z;

        rotation = Rotation;

       // rotation = playertransform.rotation.y;

        ITInventory = invent.ITInventoryArr;
        QTYInventory = invent.QTYInventoryArr;
        WieldedWeapon = Weapon;

        Health = plstats.Health;
        Magix = plstats.Magix;
        Scene = sceneindex;

        Combatxp = plLevels.CombatXP;
        Magixxp = plLevels.MagixXP;
        Miningxp = plLevels.MiningXP;
        Treecuttingxp = plLevels.TreecuttingXP;
        Explorationxp = plLevels.ExplorationXP;
        Realmgold = plLevels.RealmGold;

        //Debug.Log("Num Of Quests: " + Quests.Count);
        NumOfQuests = Quests.Count;
        if (Quests.Count != 0)
        {
            QuestNames = new string[Quests.Count];
            QuestDescriptions = new string[Quests.Count];
            QuestCompletions = new bool[Quests.Count];

            for (int index = 0; index < Quests.Count; index++)
            {
                Debug.Log(Quests.Count);
                Debug.Log(Quests[index].Title);
                Quest currentQuest = Quests[index];
                QuestNames[index] = currentQuest.Title;
                QuestDescriptions[index] = currentQuest.Description;
                QuestCompletions[index] = currentQuest.isCompleted;
            }
        }
        else
        {
            QuestNames = null;
            QuestDescriptions = null;
            QuestCompletions = null;
        }

    }

}
