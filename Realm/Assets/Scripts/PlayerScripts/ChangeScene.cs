﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeScene : MonoBehaviour
{

    public Vector3 StartPos;
    public float StartRot;
    public int nextIndex;


    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            SaveSystem.SavePlayer(nextIndex, StartPos, StartRot);
            StartCoroutine(Change());
        }
    }

    IEnumerator Change()
    {
        GUIManager.instance.UICanvas.GetComponent<TransitionLayer>().FadeOut();
        yield return new WaitForSeconds(2);
        SceneManager.LoadScene(nextIndex);

    }
}
