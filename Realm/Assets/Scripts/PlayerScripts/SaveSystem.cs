﻿using UnityEngine;
using System.IO;
using UnityEngine.SceneManagement;
using System.Runtime.Serialization.Formatters.Binary;

public static class SaveSystem
{
    public static void SavePlayer(int SceneIndex, Vector3 PlayerSavePos, float PlayerSaveRot)
    {
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/player.realm";
        FileStream stream = new FileStream(path, FileMode.Create);
        GameObject player = PlayerManager.instance.Player;

        CollectSaveData data = new CollectSaveData(player.transform.Find("Inventory").GetComponent<InventorySystem>(), player.GetComponent<Stats>(), player.GetComponent<PlayerLevels>(), PlayerSavePos, player.GetComponent<QuestCollection>().ReturnList(), SceneIndex, player.GetComponent<PlayerMovement>().currentWield, PlayerSaveRot);

        formatter.Serialize(stream, data);

        Debug.Log(path);
        stream.Close();
        OutputGUI.ShowPlayer("Data has been saved.");
    }

    public static CollectSaveData LoadPlayer()
    {
        string path = Application.persistentDataPath + "/player.realm";
        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            CollectSaveData data = formatter.Deserialize(stream) as CollectSaveData;
            stream.Close();

            return data;
        }
        else
        {
            Debug.LogError("Save file not found in" + path);
            return null;
        }
    }

    public static void LoadGame()
    {
        CollectSaveData data = SaveSystem.LoadPlayer();
        if (CheckSceneSave(data))
        {
            GameObject player = PlayerManager.instance.Player;

            //loading in player stats.
            Stats playerstats = player.transform.GetComponent<Stats>();
            playerstats.Health = data.Health;
            playerstats.Magix = data.Magix;

            //loading in player levels.
            PlayerLevels playerlevels = player.transform.GetComponent<PlayerLevels>();
            playerlevels.CombatXP = data.Combatxp;
            playerlevels.MagixXP = data.Magixxp;
            playerlevels.MiningXP = data.Miningxp;
            playerlevels.TreecuttingXP = data.Treecuttingxp;
            playerlevels.ExplorationXP = data.Explorationxp;
            playerlevels.UpdateLevels();
            playerlevels.UpdateMagixUI();
            playerlevels.RealmGold = data.Realmgold;
            playerlevels.UpdateGoldUI();

            //loading in player inventory.
            InventorySystem plInventory = player.transform.Find("Inventory").GetComponent<InventorySystem>();
            plInventory.ITInventoryArr = data.ITInventory;
            plInventory.QTYInventoryArr = data.QTYInventory;
            plInventory.attachItemToHand(data.WieldedWeapon, true, -1); //-1 fails to drop anything from the player inventory so the player loses no items.
            plInventory.UpdateImages();

            //positioning player
            Vector3 newPosition = new Vector3(data.position[0], data.position[1], data.position[2]);
            Vector3 newRotation = new Vector3(0f, data.rotation, 0f);
            //        Vector3 newRotation = Quaternion.Euler(0, data.rotation, 0);
            //Debug.Log(newRotation);
            //Debug.Log(data.rotation);
            player.GetComponent<Stats>().SetTransform(newPosition, newRotation);


            //Debug.Log(data.position[0] + " " + data.position[1] + " " + data.position[2]);

            Debug.Log("Loaded save data");
            OutputGUI.ShowPlayer("Save data loaded.");
        }
    }

    private static bool CheckSceneSave(CollectSaveData plData)
    {
        if (SceneManager.GetActiveScene().buildIndex == plData.Scene)
        {
            return true;
        }
        else
        {
            Debug.Log("Player is in the wrong scene, changing scene..");
            SceneManager.LoadScene(plData.Scene);
            return false;
        }
    }
}

