﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyController : MonoBehaviour
{

    public float lookRadius = 10f;
    public string Drop = "DemonTooth";
    public float AttackRadius = 2.5f;
    public int GoldReward = 10;
    bool isDead = false;

    Transform target;
    NavMeshAgent agent;
    Animator anim;
    Stats mobstat;
    

    // Start is called before the first frame update
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        target = PlayerManager.instance.Player.transform;
        anim = this.transform.Find("Humanoid").GetComponent<Animator>();
        mobstat = this.GetComponent<Stats>();
    }

    // Update is called once per frame

    void Die()
    {
        anim.SetLayerWeight(1, 0);
        anim.SetBool("IsChasing", false);
        anim.SetBool("IsDead", true);
        anim.SetBool("isAttacking", false);
        PlayerManager.instance.Player.transform.Find("Inventory").GetComponent<InventorySystem>().Pickup(Drop, null, null);
        PlayerManager.instance.Player.GetComponent<PlayerLevels>().GiveGold(GoldReward);
        StartCoroutine(Cleanup());
    }

    void FaceTarget()
    {
        Vector3 direction = (target.position - transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 5f);
    }

    void Update()
    {
        float distance = Vector3.Distance(target.position, transform.position);
        if(mobstat.Health <= 0 & !isDead)
        {
            isDead = true;
            Die();
        }
        if (distance <= lookRadius & !isDead)
        {
            if (distance <= AttackRadius)
            {
                //is now at a range for attacking.
            //    agent.isStopped = true;
                anim.SetBool("IsChasing", false);
                anim.SetLayerWeight(1, 1);
                anim.SetBool("isAttacking", true);
                FaceTarget();
            }
            else
            {
                anim.SetBool("IsChasing", true);
                anim.SetBool("isAttacking", false);
                anim.SetLayerWeight(1, 0);
                agent.isStopped = false;
                agent.SetDestination(target.position);
            }
        }
        else
        {
            anim.SetLayerWeight(1, 0);
            agent.isStopped = true;
            anim.SetBool("IsChasing", false);
        }
    }


    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, lookRadius);
    }

    IEnumerator Cleanup()
    {
        Debug.Log("started");
        yield return new WaitForSeconds(5);
        OutputGUI.ShowPlayer("You've been awarded with " + GoldReward);
        Destroy(this.gameObject);
    }
}
