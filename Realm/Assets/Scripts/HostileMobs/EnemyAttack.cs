﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttack : MonoBehaviour
{
    public int EstDamage;

    // Start is called before the first frame update
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            Debug.Log("Player has been hit!");

            int damage = Random.Range(EstDamage - 5, EstDamage + 5);
            OutputGUI.ShowPlayer("you took " + damage + " damage.");

            PlayerManager.instance.Player.GetComponent<Stats>().Damage(damage);
        }
    }
}
